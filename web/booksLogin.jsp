<%-- 
    Document   : books
    Created on : 2020-4-18, 15:34:39
    Author     : lw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- 引入jquery文件-->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>
        <!-- 引入最新的 Bootstrap 核心 JavaScript 文件 -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <style type="text/css">
            #hello{
                font-family: ‘Courier New’, Courier, monospace;
                font-size: 40px;
                color: lightgreen;
                float: left;
                font-weight: bold;
                margin-top: 15px;
            }
            #world{
                font-family: ‘Courier New’, Courier, monospace;
                font-size: 40px;
                float: left;
                margin-top: 15px;


            }
            #login{
                width: 140px;
                height: 40px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: right;
                margin-top: 20px;
                margin-right: 10%;
            }
            #login-p{
                font-size: 20px;
                color: lightgreen;
                text-align: center;
                line-height: 40px;
            }
            #img2{
                width: 30px;
                height: 30px;
                float: left;
                margin-top: 25px;
                margin-left: 10%;}
            #clear{
                clear: both;
            }
            #gouwuche{
                width: 130px;
                height: 40px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: right;
                margin-top: 20px;
                margin-right: 1%;
                background-color: lightgreen
            }
            #gouwuche-p{
                font-size: 20px;
                color: #FFF;
                float: left;
                margin-left: 15px;
                line-height: 40px;
            }
            .Item{
                width: 800px;
                height: 270px;
                border: 2px solid lightgreen;
                border-radius:4px;
                margin-left: 200px;
                margin-top: 10px;
            }
            .Item img{
                width: 250px;
                height: 260px;
                float: left;
                max-width:100%;
                padding:4px;
                line-height:1.42857143;
                background-color:#fff;
                
                -webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;transition:all .2s ease-in-out;
            }
            .book-p{
                font-size: 24px;
                
            }
            .index-gouwuche{
                width: 130px;
                height: 40px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: right;
                
                margin-right: 20px;;
                background-color: lightgreen
            }
            .index-gouwuche-p{
                font-size: 20px;
                color: #FFF;
                float: left;
                margin-left: 15px;
                line-height: 40px;
            }
        </style>
    </head>
    <body>

        <img id="img2" src="img/logo.png">
        <div id="hello">Hello</div><div id="world">World</div>

        <a href="Person.jsp"><div id="login"><div id="login-p">个人中心</div></div></a>
        <div id="gouwuche"><img src="img/gouwuche.png" style="width: 30px; height: 30px; float: left; margin-left: 5px; margin-top: 2px;"><p id="gouwuche-p">购物车</p></div>
        <div id="clear"></div>
        <hr>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="2000">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="images/lunbo2.jpg"  style="width: 100%; height: 400px;">
                    <div class="carousel-caption">

                    </div>
                </div>
                <div class="item">
                    <img src="images/lunbo3.jpg" style="width: 100%; height: 400px;">
                    <div class="carousel-caption">

                    </div>
                </div>
                <div class="item">
                    <img src="images/17.jpg" style="width: 100%; height: 400px;">
                    <div class="carousel-caption">

                    </div>
                </div>

            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="allbooks">
            <%
                //获取请求对象中的属性all_movies，属性值是一个Movie对象列表

            %>
            <div class="Item">
                <div>
                    <img src="img/book1.jpg" >
                </div>
                <p class="book-p">
                    &nbsp;&nbsp;&nbsp;书名：
                </p>
                <p class="book-p">
                    &nbsp;&nbsp;&nbsp;作者：
                </p>
                <p class="book-p">
                   &nbsp;&nbsp;&nbsp;价格：
                </p>
                <p class="book-p">
                    &nbsp;&nbsp;&nbsp;类型：
                </p>
                <p class="book-p">
                    &nbsp;&nbsp;&nbsp;库存：
                </p>
                <div class="index-gouwuche"><p class="index-gouwuche-p">加入购物车<p></div>
            </div>
            <%
                
            %>

        </div>
    </body>
</html>

