<%-- 
    Document   : newjsp1
    Created on : 2020-4-20, 11:18:25
    Author     : lw
--%>

<%@page import="com.helloworld.entity.Book"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="js/jquery-3.4.1.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/bootstrap.min.js"></script>
        <style type="text/css">
            #img1{
                position: absolute;
                top:22%;
                right: 12%;
                width: 400px;
                height: 400px;

            }
            #hello{
                font-family: ‘Courier New’, Courier, monospace;
                font-size: 40px;
                color: lightgreen;
                float: left;
                font-weight: bold;
                margin-top: 15px;
            }
            #world{
                font-family: ‘Courier New’, Courier, monospace;
                font-size: 40px;
                float: left;
                margin-top: 15px;
            }
            #login{
                width: 100px;
                height: 40px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: right;
                margin-top: 20px;
                margin-right: 10%;
            }
            #login-p{
                font-size: 20px;
                color: lightgreen;
                text-align: center;
                line-height: 40px;
            }
            #img2{
                width: 30px;
                height: 30px;
                float: left;
                margin-top: 25px;
                margin-left: 10%;
            }
            #clear{
                clear: both;
            }



            #gouwuche{
                width: 130px;
                height: 40px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: right;
                margin-top: 20px;
                margin-right: 1%;
                background-color: lightgreen
            }
            #gouwuche-p{
                font-size: 20px;
                color: #FFF;
                float: left;
                margin-left: 15px;
                line-height: 40px;
            }
            .wrap {
                width: 400px;
                height: 400px;
                position: relative;
                margin-left: 250px;
                margin-top: 100px;
            }

            .inner {
                /* 镜片宽度x/小图宽度=显示大图宽度/大图实际宽度 */
                /* x/250=300/500 */
                width: 150px;
                height: 150px;
                background-color: rgba(0, 0, 0, 0.5);
                position: absolute;
                top: 0;
                left: 0;
                display: none;
            }

            .big {
                width: 500px;
                height: 500px;
                position: absolute;
                bottom: 1;
                left: 270px;
                top: -20px;
                overflow: hidden;
                display: none;
            }
            .jiaru{
                width: 200px;
                height: 45px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: left;
                margin-left: 90px;
                background-color: #fff;

            }
            .jiaru-p{
                font-size: 20px;
                color: lightgreen;
                float: left;
                margin-left: 15px;
                line-height: 40px;
            }
            .goumai{
                width: 150px;
                height: 45px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: left;

                margin-left: 30px;
                background-color: lightgreen
            }
            .goumai-p{
                font-size: 20px;
                color: #FFF;
                text-align: center;

                line-height: 40px;
            }
            .shoucang{
                width: 50px;
                height: 50px;
                background-color: #fff;
                border: 0 solid #FFF;
                margin-left: 390px; margin-top: 30px; float: left;
            }
        </style>
    </head>
    <body background="" style=" background-repeat:no-repeat ; background-size:100% 100%; background-attachment: fixed;">
        <%
            //获取请求中名为movie的属性，属性值是一个Movie对象，包含了一个电影的所有信息
            Book b = (Book) request.getAttribute("book");
            //这里判断了一下电影的originalName是否为null，如果为null，显示为空字符串，否则显示实际名称，这里是为了避免页面显示null字样

        %>
        <div  id="bg" ></div>
        <img id="img2" src="img/logo.png">
        <div id="hello">Hello</div><div id="world">World</div> 
        <a href=""><div id="login"><div id="login-p">登录</div></div></a>
        <a href=""><div id="gouwuche"><img src="img/gouwuche.png" style="width: 30px; height: 30px; float: left; margin-left: 5px; margin-top: 2px;"><p id="gouwuche-p">购物车</p></div></a>
        <div id="clear"></div>
        <hr>
        <div class="">
            <div id="list_box">
                <div class="wrap">
                    <!-- 小图片 -->
                    <img src="img/<%=b.getBookid()%>1.jpg" style="width: 300px; height: 400px; margin-left: 100px;">
                    <!-- 小图片内的镜片 -->
                    <div class="inner"></div>
                    <!-- 大图片显示区域 -->
                    <div class="big">
                        <!-- 大图片 -->
                        <img src="img/<%=b.getBookid()%>1.jpg" style="width: 500px; height: 500px;">
                    </div>
                </div>
            </div>       
        </div>    


        <div style="position: absolute; top: 190px; left: 700px; width: 600px; height: 400px; ">
            <p style="font-size: 22px; text-align: center; font-weight: 700;">《<%=b.getBookname()%>》</p>
            <br>
            <div><span style="font-size: 18px; color:#737373; margin-left: 30px;">价格：</span> <span style="font-size: 18px;"><%=b.getBookprice()%></span></div>
            <br>
            <div><span style="font-size: 18px; color:#737373; margin-left: 30px;">作者：</span> <span style="font-size: 18px;"><%=b.getBookauthor()%></span></div>
            <br>
            <div><span style="font-size: 18px; color:#737373; margin-left: 30px;">类型：</span> <span style="font-size: 18px;"><%=b.getBooktype()%></span></div>
            <br>
            <div><span style="font-size: 18px; color:#737373; margin-left: 30px;">销量：</span> <span style="font-size: 18px; color: red; font-weight: 600;">999+</span></div>
            <br>
            <form action="aos" class="form-inline">
                <b style="margin-left: 30px; font-size: 18px; color:#737373;">数量：</b>
                <input name="number" type="number" min="1" name="qty" value="1" class="form-control" style="width: 60px!important; margin-left: 10px;">
                <b style="margin-left: 10px; font-size: 18px; color:#ccc;">本</b>
                <input name="bookId" type="hidden" value="<%=b.getBookid()%>">
                <br><br>
                <br><br><br><br>
                <button class="jiaru">
                    <img src="img/gouwuche2.png" style="width: 30px; height: 30px; float: left; margin-left: 5px; margin-top: 3px;">
                    <p class="jiaru-p">加入购物车</p>
                </button>
                <button class="goumai">
                    <p class="goumai-p">立刻购买</p>
                </button>


            </form>
        </div>
        <span><button class="shoucang"><img id="img5" src="img/shoucang1.png" style="width: 40px; height: 40px;" /></button></span><div style="float: left; margin-left: 10px;  margin-top: 40px; font-size: 16px;">收藏该商品</div>
        <div id="clear"></div>
        <br>
        <div class="container">
        
        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#home" data-toggle="tab">
                    书籍详情
                </a>
            </li>
            <li><a href="#ios" data-toggle="tab">评论</a></li>

        </ul></div>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="home">
                 <img src="img/<%=b.getBookid()%>2.jpg" style="width: 1150px; margin-left: 270px; ">
            </div>
            <div class="tab-pane fade" id="ios">
                <p>iOS 是一个由苹果公司开发和发布的手机操作系统。最初是于 2007 年首次发布 iPhone、iPod Touch 和 Apple 
                    TV。iOS 派生自 OS X，它们共享 Darwin 基础。OS X 操作系统是用在苹果电脑上，iOS 是苹果的移动版本。</p>
            </div>

        </div>
        <script>
            var wrap = document.querySelector('.wrap');
            var inner = document.querySelector('.inner');
            var big = document.querySelector('.big');
            var bigImg = document.querySelector('.big img');
            // 鼠标移动事件
            wrap.onmousemove = function (e) {
                var e = window.event || e;
                // 获取元素相对于浏览器窗口的坐标e.clientX-镜片一半的距离-相对于body的偏移量
                var left = e.clientX - inner.offsetWidth / 2 - wrap.offsetLeft;
                var top = e.clientY - inner.offsetHeight / 2 - wrap.offsetTop;
                // console.log(left, top);
                // 控制镜片移动的最大边界；
                var bigWidth = wrap.offsetWidth - inner.offsetWidth;
                var bigHeight = wrap.offsetHeight - inner.offsetHeight;
                // console.log(bigWidth, bigHeight);
                // 如果鼠标移动的最大值大于最大边界，限定最大值为最大边界；
                if (left > bigWidth) {
                    left = bigWidth;
                }
                if (left < 0) {
                    left = 0;
                }
                if (top > bigHeight) {
                    top = bigHeight;
                }
                if (top < 0) {
                    top = 0;
                }
                // 控制镜片的位置
                inner.style.left = left + "px";
                inner.style.top = top + 'px';
                // 控制大图片移动相对位置(-大图的宽度/小图的宽度*镜片的偏移量)
                bigImg.style.marginLeft = -500 / 250 * inner.offsetLeft + 'px';
                bigImg.style.marginTop = -500 / 250 * inner.offsetTop + 'px';
            }
            wrap.onmouseenter = function () {
                inner.style.display = 'block';
                big.style.display = 'block';
            }
            wrap.onmouseleave = function () {
                inner.style.display = 'none';
                big.style.display = 'none';
            }
            $("span").click(function () {
                var src = "img/shoucang2.png";    //新图片地址
                $(this).find("#img5").attr("src", src);    //更换图片地址
            });
        </script>



    </body>

</html>


