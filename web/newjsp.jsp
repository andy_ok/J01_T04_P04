<%-- 
    Document   : newjsp
    Created on : 2020-4-18, 15:44:17
    Author     : lw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        #hello{
                font-family: ‘Courier New’, Courier, monospace;
                font-size: 40px;
                color: lightgreen;
                float: left;
                font-weight: bold;
                margin-top: 15px;
            }
            #world{
                font-family: ‘Courier New’, Courier, monospace;
                font-size: 40px;
                float: left;
                margin-top: 15px;


            }
            #login{
                width: 100px;
                height: 40px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: right;
                margin-top: 20px;
                margin-right: 10%;
            }
             #login-p{
                font-size: 20px;
                color: lightgreen;
                text-align: center;
                line-height: 40px;
            }
            #img2{
                width: 30px;
                height: 30px;
                float: left;
                margin-top: 25px;
                margin-left: 10%;}
            #clear{
                clear: both;
            }
            #gouwuche{
                width: 130px;
                height: 40px;
                border: 2px solid lightgreen;
                border-radius: 4px;
                float: right;
                margin-top: 20px;
                margin-right: 1%;
                background-color: lightgreen
            }
            #gouwuche-p{
                font-size: 20px;
                color: #FFF;
                float: left;
                margin-left: 15px;
                line-height: 40px;
            }
       

        .wrap {
            width: 250px;
            height: 250px;
            position: relative;
            margin-left: 100px;
            margin-top: 100px;
        }

        .inner {
        	/* 镜片宽度x/小图宽度=显示大图宽度/大图实际宽度 */
            /* x/250=300/500 */
            width: 150px;
            height: 150px;
            background-color: rgba(0, 0, 0, 0.5);
            position: absolute;
            top: 0;
            left: 0;
            display: none;
        }

        .big {
            width: 500px;
            height: 500px;
            position: absolute;
            left: 270px;
            top: -20px;
            overflow: hidden;
            display: none;
        }

    </style>
</head>

<body>
    <img id="img2" src="img/logo.png">
        <div id="hello">Hello</div><div id="world">World</div>

        <div id="login"><div id="login-p">登录</div></div>
        <div id="gouwuche"><img src="img/gouwuche.png" style="width: 30px; height: 30px; float: left; margin-left: 5px; margin-top: 2px;"><p id="gouwuche-p">购物车</p></div>
        <div id="clear"></div>
        <hr>
   <div id="list_box">
       <div class="wrap">
        <!-- 小图片 -->
        <img src="img/book1.jpg">
        <!-- 小图片内的镜片 -->
        <div class="inner"></div>
        <!-- 大图片显示区域 -->
        <div class="big">
            <!-- 大图片 -->
            <img src="img/book1.jpg" style="width: 500px; height: 500px;">
        </div>
    </div>

    </div>    <script>
       var wrap = document.querySelector('.wrap');
        var inner = document.querySelector('.inner');
        var big = document.querySelector('.big');
        var bigImg = document.querySelector('.big img');
        // 鼠标移动事件
        wrap.onmousemove = function (e) {
            var e = window.event || e;
            // 获取元素相对于浏览器窗口的坐标e.clientX-镜片一半的距离-相对于body的偏移量
            var left = e.clientX - inner.offsetWidth / 2 - wrap.offsetLeft;
            var top = e.clientY - inner.offsetHeight / 2 - wrap.offsetTop;
            // console.log(left, top);
            // 控制镜片移动的最大边界；
            var bigWidth = wrap.offsetWidth - inner.offsetWidth;
            var bigHeight = wrap.offsetHeight - inner.offsetHeight;
            // console.log(bigWidth, bigHeight);
            // 如果鼠标移动的最大值大于最大边界，限定最大值为最大边界；
            if (left > bigWidth) {
                left = bigWidth;
            }
            if (left < 0) {
                left = 0;
            }
            if (top > bigHeight) {
                top = bigHeight;
            }
            if (top < 0) {
                top = 0;
            }
            // 控制镜片的位置
            inner.style.left = left + "px";
            inner.style.top = top + 'px';
            // 控制大图片移动相对位置(-大图的宽度/小图的宽度*镜片的偏移量)
            bigImg.style.marginLeft = -500 / 250 * inner.offsetLeft + 'px';
            bigImg.style.marginTop = -500 / 250 * inner.offsetTop + 'px';
        }
        wrap.onmouseenter = function () {
            inner.style.display = 'block';
            big.style.display = 'block';
        }
        wrap.onmouseleave = function () {
            inner.style.display = 'none';
            big.style.display = 'none';
        }

    </script>
</body>

</html>
