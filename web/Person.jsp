<%-- 
    Document   : Person
    Created on : 2020-4-27, 10:22:57
    Author     : lw
--%>

<%@page import="com.helloworld.entity.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- 引入jquery文件-->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>
        <!-- 引入最新的 Bootstrap 核心 JavaScript 文件 -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <style type="text/css">
            #hello{
                font-family: ‘Courier New’, Courier, monospace;
                font-size: 40px;
                color: lightgreen;
                float: left;
                font-weight: bold;
            }
            #world{
                font-family: ‘Courier New’, Courier, monospace;
                font-size: 40px;
                float: left;

            }

            #img2{
                width: 30px;
                height: 30px;
                float: left;
                margin-top: 10px;
                margin-left: 10%;}
            #clear{
                clear: both;
            }
            .beijing{
                width: 100%;
                height: 400px;
                background-color: lightgreen;
            }
            #login{
                width: 250px;
                height: 250px;
                border-radius: 50%;
                margin-top: 75px;
                float: left;
                margin-left: 170px;
            }
            #geren{
                float: left;
                margin-left: 120px;
                margin-top: 75px;
                width: 200px;
                height: 200px;

            }
            #username{
                font-size: 48px;
                float: left;
                margin-left: 30px;
            }
            #bianji{
                width: 190px;
                height: 50px;
                border: 2px solid #000;
                margin-top: 180px;
                border-radius: 5px;
            }
            #bianji-p{
                font-size: 20px;
                text-align: center;
                line-height: 50px;
                color: #000;
            }
            #shoucang{
                width: 250px;
                height: 250px;
                border: 3px solid lawngreen;
                border-radius: 10px;
                float: left;
                margin-left: 170px;
                background-color: #FFF;
            }
            #gouwuche{
                width: 250px;
                height: 250px;
                border: 3px solid lawngreen;
                border-radius: 10px;
                float: left;
                margin-left: 120px;
            }
            #dingdan{
                width: 250px;
                height: 250px;
                border: 3px solid lawngreen;
                border-radius: 10px;
                float: left;
                margin-left: 120px;
            }
            #pingjia{
                width: 250px;
                height: 250px;
                border: 3px solid lawngreen;
                border-radius: 10px;
                float: left;
                margin-left: 120px;
            }
        </style>
    </head>
    <body>
        <%
            User u = (User) request.getAttribute("user");
        %>
        <img id="img2" src="img/logo.png">
        <div id="hello">Hello</div><div id="world">World</div>
        <div id="clear"></div>
        <hr>
        <div class="beijing">
            <img src="img/login.jpg" id="login">
            <div id="geren">

                <div id="username">
                    <%=u.getUsername()%>
                </div>

                <a href="" style="text-decoration: none;"><div id="bianji"><div id="bianji-p">编辑个人资料</div></div></div></a>
    </div>
    <div id="clear"></div>
    <br>
    <form action="index">
    <button  id="shoucang">
        <img src="img/shoucang2(1).png" style="width: 150px; height: 150px; margin-top: 20px;">
        <div style="font-size: 32px; color:lawngreen; margin-top: 10px; ">收&nbsp;&nbsp;藏</div>
    </button></form>
    <div id="gouwuche">
        <img src="img/gouwuche4.png" style="width: 150px; height: 150px; margin-left: 50px; margin-top: 20px;">
        <div style="font-size: 32px; color:lawngreen; margin-top: 10px; margin-left: 70px;">购&nbsp;物&nbsp;车</div>
    </div>
    <div id="dingdan">
        <img src="img/dingdan.png" style="width: 150px; height: 150px; margin-left: 50px; margin-top: 20px;">
        <div style="font-size: 32px; color:lawngreen; margin-top: 10px; margin-left: 80px;">订&nbsp;&nbsp;单</div>
    </div>
    <div id="pingjia">
        <img src="img/pingjia.png" style="width: 150px; height: 150px; margin-left: 50px; margin-top: 20px;">
        <div style="font-size: 32px; color:lawngreen; margin-top: 10px; margin-left: 80px;">评&nbsp;&nbsp;价</div>
    </div>
</body>
</html>
