/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.service.impl;

import com.helloworld.dao.BookDao;
import com.helloworld.dao.impl.BookDaoImpl;
import com.helloworld.entity.Book;
import com.helloworld.service.BookService;
import java.util.List;

/**
 *
 * @author lw
 */
public class BookServiceImpl implements BookService{
    BookDao bookDao = new BookDaoImpl();
    @Override
    public List<Book> findAllBooks(){
        return bookDao.getAllBooks();
    }

    @Override
    public List<Book> findBooksByKeyword(String keyword) {
        return bookDao.getBooksByKeyword(keyword);
    }

    @Override
    public Book findBookById(String bookid) {
        Book book = bookDao.getBookInfoById(bookid);
        
        
        return book;
    }
    @Override
    public List<Book> findOrderedBooks() {
        return bookDao.getOrderedBooks();
    }
}
