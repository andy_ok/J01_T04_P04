
package com.helloworld.service.impl;

import com.helloworld.dao.UserDao;
import com.helloworld.dao.impl.UserDaoImpl;
import com.helloworld.entity.User;
import java.util.List;
import com.helloworld.service.UserService;

/**
 *
 * @author 123
 */
public class UserServiceImpl implements UserService{
    
    UserDao userDao=new UserDaoImpl();

    @Override
    public User validateLogin(String username, String password) {
        User u=userDao.getOneById(username);
        if(null!=u&&u.getPassword().equals(password))
            return u;
        else
            return null;
    }

    

    @Override
    public int registerNewUser(User u) {
        User stu=userDao.getOneById(u.getUsername());
        if(null==stu){
            return userDao.insert(u);
        }else{
            return -1;
        }
    }

    
    
}
