/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.service.impl;

import com.helloworld.dao.CommentDao;
import com.helloworld.dao.impl.CommentDaoImpl;
import com.helloworld.entity.Comment;
import com.helloworld.service.CommentService;
import java.util.List;

/**
 *
 * @author lw
 */
public class CommentServiceImpl implements CommentService{
    CommentDao commentdao=new CommentDaoImpl();
        
    @Override
    public List<Comment> findBookComments() {
       return commentdao.getBookComments();
    }
    
    
}
