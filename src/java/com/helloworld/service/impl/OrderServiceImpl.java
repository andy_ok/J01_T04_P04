/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.service.impl;

import com.helloworld.dao.OrderDao;
import com.helloworld.dao.impl.OrderDaoImpl;
import com.helloworld.entity.Order;
import com.helloworld.service.OrderService;

/**
 *
 * @author 123
 */
public class OrderServiceImpl implements OrderService{

    OrderDao orderDao=new OrderDaoImpl();
    
    @Override
    public int addNewOrder(Order o) {
       
         return orderDao.insert(o);
    }
    
}
