/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.service;

import com.helloworld.entity.Book;
import java.util.List;

/**
 *
 * @author lw
 */
public interface BookService {
    List<Book> findAllBooks();
    List<Book> findBooksByKeyword(String keyword);
    Book findBookById(String movieId);
     List<Book> findOrderedBooks();
}
