/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.entity;

/**
 *
 * @author lw
 */
public class Book {
   
    private String bookid;
    private String bookname;
    private String bookauthor;
    private float bookprice;
    private String booktype;
    private String bookimg;
    private int number;
    
    public Book(String bookid, String bookname, String bookauthor, float bookprice, String booktype, String bookimg) {
        this.bookid = bookid;
        this.bookname = bookname;
        this.bookauthor = bookauthor;
        
        this.bookprice = bookprice;
        this.booktype = booktype;
        this.bookimg = bookimg;
    }


    public Book(String bookid, String bookname, String bookauthor, float bookprice, String booktype, String bookimg, int number) {
        this.bookid = bookid;
        this.bookname = bookname;
        this.bookauthor = bookauthor;
        
        this.bookprice = bookprice;
        this.booktype = booktype;
        this.bookimg = bookimg;
        this.number = number;
    }
    
    

    public String getBookid() {
        return bookid;
    }

    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    
    

    

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getBookauthor() {
        return bookauthor;
    }

    public void setBookauthor(String bookauthor) {
        this.bookauthor = bookauthor;
    }

    public float getBookprice() {
        return bookprice;
    }

    public void setBookprice(float bookprice) {
        this.bookprice = bookprice;
    }

    public String getBookimg() {
        return bookimg;
    }

    public void setBookimg(String bookimg) {
        this.bookimg = bookimg;
    }

    public String getBooktype() {
        return booktype;
    }

    public void setBooktype(String booktype) {
        this.booktype = booktype;
    }
    
    public int getBooknum() {
        return number;
    }

    public void setBooknum(int number) {
        this.number = number;
    }

    
    
    
}
