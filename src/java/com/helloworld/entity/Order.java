/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.entity;

/**
 *
 * @author 123
 */
public class Order {
    private String userName;
    private String bookId;
    private String number;

    public Order(String userName, String bookId, String number) {
        this.userName = userName;
        this.bookId = bookId;
        this.number = number;
    }

    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    
}
