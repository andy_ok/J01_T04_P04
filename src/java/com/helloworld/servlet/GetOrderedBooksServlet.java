/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.servlet;

import com.helloworld.entity.Book;
import com.helloworld.service.BookService;
import com.helloworld.service.impl.BookServiceImpl;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lw
 */
@WebServlet("/cart")
public class GetOrderedBooksServlet extends HttpServlet{
    BookService bookService=new BookServiceImpl();
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        
        //2. 执行业务逻辑 
        List<Book> list=bookService.findOrderedBooks(); //查询订单中书的信息
        
        
        System.out.println(list.size());
        //3. 生成动态响应
        //将书列表作为一个属性添加到请求对象中，转发请求共享给cart.jsp页面
        req.setAttribute("ordered_books", list); 
      req.getRequestDispatcher("/cart.jsp").forward(req, resp);
    }
}
