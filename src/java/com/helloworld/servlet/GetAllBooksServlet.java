/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.servlet;

import com.helloworld.entity.Book;
import com.helloworld.service.BookService;
import com.helloworld.service.impl.BookServiceImpl;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lw
 */
@WebServlet("/index")
public class GetAllBooksServlet extends HttpServlet{
    BookService bookService=new BookServiceImpl();
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //1. 因为查询所有电影，无需请求参数，所以不获取请求参数
        
        //2. 执行业务逻辑 
        List<Book> list=bookService.findAllBooks(); //查询所有电影的信息
        
        //3. 生成动态响应
        //将电影列表作为一个属性添加到请求对象中，转发请求共享给index.jsp页面
        req.setAttribute("all_books", list); 
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
