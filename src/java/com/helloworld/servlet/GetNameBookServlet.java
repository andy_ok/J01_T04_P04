/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.servlet;

import com.helloworld.entity.Book;
import com.helloworld.service.BookService;
import com.helloworld.service.impl.BookServiceImpl;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lw
 */
@WebServlet("/gnbs1")
public class GetNameBookServlet extends HttpServlet{
     BookService bookService=new BookServiceImpl();
     
     protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     String keyword = req.getParameter("keyword");

        //2. 执行业务逻辑 
        //调用MovieService类的findMoviesByKeyword()方法根据关键字查询电影列表
        //但是并没有查询电影的所有信息，只是查询了基本信息
        List<Book> list = bookService.findBooksByKeyword(keyword);
       
        //3. 生成响应内容
        req.setAttribute("book_list", list);
        req.getRequestDispatcher("/book_list.jsp").forward(req, resp);
     
     }
    
    
    
}
