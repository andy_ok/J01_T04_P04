/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.servlet;

import com.helloworld.entity.Order;
import com.helloworld.service.OrderService;
import com.helloworld.service.impl.OrderServiceImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author 123
 */
@WebServlet("/aos")
public class AddOrderServlet extends HttpServlet{
    
    OrderService orderService=new OrderServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         req.setCharacterEncoding("utf-8");
        
         String userName="123456";
         String bookId=req.getParameter("bookId");
         String number=req.getParameter("number");
         
         Order newOrder=new Order(userName,bookId,number);
         
         int s=orderService.addNewOrder(newOrder);
         
//        resp.getWriter().println(s);
//        resp.sendRedirect("success.html");
         JOptionPane.showMessageDialog(null, "最基本提示框", "Title",JOptionPane.PLAIN_MESSAGE);
         
    }
    
}
