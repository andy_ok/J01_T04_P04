package com.helloworld.servlet;

import com.helloworld.entity.User;
import com.helloworld.service.UserService;
import com.helloworld.service.impl.UserServiceImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 该Servlet负责处理添加新学生的请求
 *
 * @author Anna
 */
@WebServlet("/aus")
public class AddUserServlet extends HttpServlet {

    UserService userService=new UserServiceImpl();
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

 
        req.setCharacterEncoding("utf-8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        

        User newUser = new User(username, password);

        int s=userService.registerNewUser(newUser);
        
        resp.getWriter().println(s);
        
    }
}
