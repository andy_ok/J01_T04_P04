/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.servlet;

import com.helloworld.entity.User;
import com.helloworld.service.UserService;
import com.helloworld.service.impl.UserServiceImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lw
 */
@WebServlet("/lg")
public class LoginServlet extends HttpServlet {

    UserService userService=new UserServiceImpl();
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

 
        req.setCharacterEncoding("utf-8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        

       

        User user=userService.validateLogin(username,password);
        
        if(user!=null){
             
//             resp.sendRedirect("booksLogin.jsp");
             req.setAttribute("user", user);
             req.getRequestDispatcher("/Person.jsp").forward(req, resp);
            
        }else{
            resp.sendRedirect("index.html");
        }
       
        
    }
}
