/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DatabaseUtil,一个数据库工具类，打开和关闭数据库
 *
 * @author 123
 */
public class DatabaseUtil {

    private static final String DRIVER_CLASS;
    private static final String URL;
    private static final String USERNAME;
    private static final String PASSWORD;
    private static final ResourceBundle RB=ResourceBundle.getBundle("com.helloworld.util.dbconfig");
    
    static{
        DRIVER_CLASS=RB.getString("jdbc.driver");
        URL=RB.getString("jdbc.url");
        USERNAME=RB.getString("jdbc.username");
        PASSWORD=RB.getString("jdbc.password");
    }
/**
 * 打开一个数据库连接
 * @return 一个Connection对象，表示一个数据库连接
 */
    public static Connection getConnection() {

        Connection con = null;

        try {
            Class.forName(DRIVER_CLASS);
            con=DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    public static void Close(ResultSet rs, Statement sm, Connection con) {

        try {
            if (null != rs) {
                rs.close();
            }
            if (null != sm) {
                sm.close();
            }
            if (null != con) {
                con.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

}
