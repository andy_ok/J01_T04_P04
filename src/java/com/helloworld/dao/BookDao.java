/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.dao;

import com.helloworld.entity.Book;
import java.util.List;

/**
 *
 * @author lw
 */
public interface BookDao {
    Book getBookInfoById(String bookid);
    List<Book> getAllBooks();
    List<Book> getBooksByKeyword(String bookname);
    List<Book> getOrderedBooks();
}
