/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.dao.impl;

import com.helloworld.dao.UserDao;
import com.helloworld.entity.User;
import com.helloworld.util.DatabaseUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 123
 */
public class UserDaoImpl extends BaseDaoImpl implements UserDao {

    @Override
    public int insert(User u) {
        return executeUpdate("insert into Users values(?,?)"
                ,u.getUsername()
                ,u.getPassword());
                
    }

    @Override
    public int update(User u) {
        return executeUpdate("update Users set Password=? where Username=?"
                
                ,u.getPassword()
                ,u.getUsername());
    }

    @Override
    public User getOneById(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User user=null;

        try {
            con = DatabaseUtil.getConnection();
            ps = con.prepareStatement("select * from Users where Username=?");
            ps.setString(1, id);
            rs = ps.executeQuery();
            if(rs.next()) {
                user = new User(rs.getString(1), rs.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.Close(rs, ps, con);
        }
        return user;
    }

    

}
