/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.dao.impl;

import com.helloworld.dao.BookDao;
import com.helloworld.dao.CommentDao;
import com.helloworld.entity.Book;
import com.helloworld.entity.Comment;
import com.helloworld.util.DatabaseUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lw
 */
public class CommentDaoImpl implements CommentDao{

    @Override
    public List<Comment> getBookComments() {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Comment> list = new ArrayList<>();
        
        try {
            con = DatabaseUtil.getConnection();
            ps = con.prepareStatement("select CommentText from Books,Comment where Books.BookId=Comment.BookId");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Comment(
                        rs.getString(1)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.Close(rs, ps, con);
        }
        return list;
    }
    
}
