/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.dao.impl;

import com.helloworld.dao.BookDao;
import com.helloworld.entity.Book;
import com.helloworld.util.DatabaseUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lw
 */
public class BookDaoImpl implements BookDao{

    @Override
    public Book getBookInfoById(String bookid) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = DatabaseUtil.getConnection();
            ps = con.prepareStatement("select BookId,BookName,BookAuthor,BookPrice,BookType,BookImg from Books where BookId=?");
            ps.setString(1, bookid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Book(
                        rs.getString(1),
                        rs.getString(2),                      
                        rs.getString(3),
                        rs.getFloat(4),
                        rs.getString(5),
                        rs.getString(6)                        
                        );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.Close(rs, ps, con);
        }
        return null;

    }

    @Override
    public List<Book> getAllBooks() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> list = new ArrayList<>();
        
        try {
            con = DatabaseUtil.getConnection();
            ps = con.prepareStatement("select BookId,BookName,BookAuthor,BookPrice,BookType,BookImg from Books");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Book(rs.getString(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.Close(rs, ps, con);
        }
        return list;
    }

    @Override
    public List<Book> getBooksByKeyword(String bookname) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> list = new ArrayList<>();
        
        try {
            con = DatabaseUtil.getConnection();
            ps = con.prepareStatement("select BookId,BookName,BookAuthor,BookPrice,BookImg,BookType from Books where BookName like ?");
            ps.setString(1, "%" + bookname + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Book(rs.getString(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.Close(rs, ps, con);
        }
        return list;
    }
    @Override
    public List<Book> getOrderedBooks() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> list = new ArrayList<>();
        
        try {
            con = DatabaseUtil.getConnection();
            ps = con.prepareStatement("select Books.BookId,BookName,BookAuthor,BookPrice,BookImg,BookType,number from Books,Orders where Books.BookId=Orders.BookId");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Book(
                        rs.getString(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.Close(rs, ps, con);
        }
        return list;
    }
    }
    

