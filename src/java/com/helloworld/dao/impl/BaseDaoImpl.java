/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.dao.impl;

import com.helloworld.dao.BaseDao;
import com.helloworld.util.DatabaseUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author 123
 */
public class BaseDaoImpl implements BaseDao{

    @Override
    public int executeUpdate(String sql, Object... params) {
        Connection con = null;
        PreparedStatement ps = null;
        int rows = 0; //受影响行数

        try {
            con = DatabaseUtil.getConnection();
            ps = con.prepareStatement(sql);
            for(int i=0;i<params.length;i++){
                ps.setObject(i+1 ,params[i]);
            }
            rows = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.Close(null, ps, con);
        }
        return rows;
    }
    
}
