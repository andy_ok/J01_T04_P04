/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.dao.impl;

import com.helloworld.dao.OrderDao;
import com.helloworld.entity.Order;

/**
 *
 * @author 123
 */
public class OrderDaoImpl extends BaseDaoImpl implements OrderDao{

    @Override
    public int insert(Order o) {
        return executeUpdate("insert into Orders values(?,?,?)",
                o.getUserName(),
                o.getBookId(),
                o.getNumber());
    }



}
