/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.dao;

/**
 *
 * @author 123
 */
public interface BaseDao {
    /**
     * 
     * @param sql 字符串表示的要执行的sql语句
     * @param params sql语句需要的各个参数
     * @return 一个整数，表示受影响的行数，受影响1行返回1，受影响0行返回0，受影响n行返回n
     */
    int executeUpdate(String sql,Object... params);
}
