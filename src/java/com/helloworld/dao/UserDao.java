/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.dao;

import com.helloworld.entity.User;

/**
 *
 * @author lw
 */
public interface UserDao extends BaseDao{
    
    int insert(User u);
    int update(User u);
    User getOneById(String username);
    
}
