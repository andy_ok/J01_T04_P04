package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Register_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>登录页面</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/login.css\">\n");
      out.write("        <style type=\"text/css\">\n");
      out.write("            #hello{\n");
      out.write("                font-family: ‘Courier New’, Courier, monospace;\n");
      out.write("                font-size: 35px;\n");
      out.write("                color: lightgreen;\n");
      out.write("                float: left;\n");
      out.write("                font-weight: bold;\n");
      out.write("            }\n");
      out.write("            #world{\n");
      out.write("                font-family: ‘Courier New’, Courier, monospace;\n");
      out.write("                font-size: 35px;\n");
      out.write("                float: left;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #img2{\n");
      out.write("                width: 25px;\n");
      out.write("                height: 25px;\n");
      out.write("                float: left;\n");
      out.write("                margin-top: 10px;\n");
      out.write("                margin-left: 63px;}\n");
      out.write("            #clear{\n");
      out.write("                clear: both;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #login-box{\n");
      out.write("                width: 400px;\n");
      out.write("                border: 1px solid #ddd;\n");
      out.write("                border-radius:30px;                              \n");
      out.write("                background-color: #FFF;\n");
      out.write("                position: absolute;\n");
      out.write("                top: 25%;\n");
      out.write("                left: 15%;\n");
      out.write("                text-align: center;\n");
      out.write("                padding: 20px;                \n");
      out.write("                box-shadow: 8px 8px 8px grey;\n");
      out.write("            }\n");
      out.write("            #img1{\n");
      out.write("                position: absolute;\n");
      out.write("                top:18%;\n");
      out.write("                right: 18%;\n");
      out.write("                width: 500px;\n");
      out.write("                height: 500px;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("\n");
      out.write("    <body background=\"img\\bg1.jpg\" style=\" background-repeat:no-repeat ; background-size:100% 100%; background-attachment: fixed;\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <div id=\"login-box\">\n");
      out.write("            \n");
      out.write("            <br>\n");
      out.write("            <img id=\"img2\" src=\"img/logo.png\">\n");
      out.write("            <div id=\"hello\">Hello</div><div id=\"world\">World</div>\n");
      out.write("            <div id=\"clear\"></div>\n");
      out.write("            <br>\n");
      out.write("            <form action=\"aus\" class=\"form-inline\" method=\"post\">\n");
      out.write("                <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"请输入用户名\" style=\"width:230px;\" > \n");
      out.write("                <br><br>\n");
      out.write("                \n");
      out.write("                <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"请输入密码\" style=\"width:230px;\">\n");
      out.write("                <br><br>\n");
      out.write("                <button class=\"btn btn-success\" style=\"width:230px;\">注册</button>\n");
      out.write("            </form>\n");
      out.write("            <hr>\n");
      out.write("            <div style=\"text-align: center;\">已有账户？<a style=\"color: lightgreen;\">登录</a></div>\n");
      out.write("            <br>\n");
      out.write("            \n");
      out.write("        </div>\n");
      out.write("<img id=\"img1\" src=\"img/book.png\">\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
