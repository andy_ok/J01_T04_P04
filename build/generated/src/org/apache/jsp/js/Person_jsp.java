package org.apache.jsp.js;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Person_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n");
      out.write("        <!-- 引入jquery文件-->\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js\"></script>\n");
      out.write("        <!-- 引入最新的 Bootstrap 核心 JavaScript 文件 -->\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <style type=\"text/css\">\n");
      out.write("            #hello{\n");
      out.write("                font-family: ‘Courier New’, Courier, monospace;\n");
      out.write("                font-size: 40px;\n");
      out.write("                color: lightgreen;\n");
      out.write("                float: left;\n");
      out.write("                font-weight: bold;\n");
      out.write("            }\n");
      out.write("            #world{\n");
      out.write("                font-family: ‘Courier New’, Courier, monospace;\n");
      out.write("                font-size: 40px;\n");
      out.write("                float: left;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("            #login{\n");
      out.write("                width: 100px;\n");
      out.write("                height: 40px;\n");
      out.write("                border: 2px solid lightgreen;\n");
      out.write("                border-radius: 4px;\n");
      out.write("                float: right;\n");
      out.write("                margin-top: 10px;\n");
      out.write("                margin-right: 10%;\n");
      out.write("            }\n");
      out.write("            #login-p{\n");
      out.write("                font-size: 20px;\n");
      out.write("                color: lightgreen;\n");
      out.write("                text-align: center;\n");
      out.write("                line-height: 40px;\n");
      out.write("            }\n");
      out.write("            #img2{\n");
      out.write("                width: 30px;\n");
      out.write("                height: 30px;\n");
      out.write("                float: left;\n");
      out.write("                margin-top: 10px;\n");
      out.write("                margin-left: 10%;}\n");
      out.write("            #clear{\n");
      out.write("                clear: both;\n");
      out.write("            }\n");
      out.write("            #gouwuche{\n");
      out.write("                width: 130px;\n");
      out.write("                height: 40px;\n");
      out.write("                border: 2px solid lightgreen;\n");
      out.write("                border-radius: 4px;\n");
      out.write("                float: right;\n");
      out.write("                margin-top: 10px;\n");
      out.write("                margin-right: 1%;\n");
      out.write("                background-color: lightgreen\n");
      out.write("            }\n");
      out.write("            #gouwuche-p{\n");
      out.write("                font-size: 20px;\n");
      out.write("                color: #FFF;\n");
      out.write("                float: left;\n");
      out.write("                margin-left: 15px;\n");
      out.write("                line-height: 40px;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("\n");
      out.write("        <img id=\"img2\" src=\"img/logo.png\">\n");
      out.write("        <div id=\"hello\">Hello</div><div id=\"world\">World</div>\n");
      out.write("   \n");
      out.write("    \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
