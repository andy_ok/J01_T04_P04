package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.helloworld.entity.Book;
import java.util.List;
import java.util.List;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this license header, choose License Headers in Project Properties.\n");
      out.write("To change this template file, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>TODO supply a title</title>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n");
      out.write("        <!-- 引入jquery文件-->\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js\"></script>\n");
      out.write("        <!-- 引入最新的 Bootstrap 核心 JavaScript 文件 -->\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <style type=\"text/css\">\n");
      out.write("            #hello{\n");
      out.write("                font-family: ‘Courier New’, Courier, monospace;\n");
      out.write("                font-size: 40px;\n");
      out.write("                color: lightgreen;\n");
      out.write("                float: left;\n");
      out.write("                font-weight: bold;\n");
      out.write("                margin-top: 15px;\n");
      out.write("            }\n");
      out.write("            #world{\n");
      out.write("                font-family: ‘Courier New’, Courier, monospace;\n");
      out.write("                font-size: 40px;\n");
      out.write("                float: left;\n");
      out.write("                margin-top: 15px;\n");
      out.write("\n");
      out.write("\n");
      out.write("            }\n");
      out.write("            #login{\n");
      out.write("                width: 100px;\n");
      out.write("                height: 40px;\n");
      out.write("                border: 2px solid lightgreen;\n");
      out.write("                border-radius: 4px;\n");
      out.write("                float: right;\n");
      out.write("                margin-top: 20px;\n");
      out.write("                margin-right: 10%;\n");
      out.write("            }\n");
      out.write("            #login-p{\n");
      out.write("                font-size: 20px;\n");
      out.write("                color: lightgreen;\n");
      out.write("                text-align: center;\n");
      out.write("                line-height: 40px;\n");
      out.write("            }\n");
      out.write("            #img2{\n");
      out.write("                width: 30px;\n");
      out.write("                height: 30px;\n");
      out.write("                float: left;\n");
      out.write("                margin-top: 25px;\n");
      out.write("                margin-left: 10%;}\n");
      out.write("            #clear{\n");
      out.write("                clear: both;\n");
      out.write("            }\n");
      out.write("            #gouwuche{\n");
      out.write("                width: 130px;\n");
      out.write("                height: 40px;\n");
      out.write("                border: 2px solid lightgreen;\n");
      out.write("                border-radius: 4px;\n");
      out.write("                float: right;\n");
      out.write("                margin-top: 20px;\n");
      out.write("                margin-right: 1%;\n");
      out.write("                background-color: lightgreen\n");
      out.write("            }\n");
      out.write("            #gouwuche-p{\n");
      out.write("                font-size: 20px;\n");
      out.write("                color: #FFF;\n");
      out.write("                float: left;\n");
      out.write("                margin-left: 15px;\n");
      out.write("                line-height: 40px;\n");
      out.write("            }\n");
      out.write("            .Item{\n");
      out.write("                width: 250px;\n");
      out.write("                height: 280px;\n");
      out.write("                border: 2px solid lightgreen;\n");
      out.write("                border-radius:4px;\n");
      out.write("                margin-left: 200px;\n");
      out.write("                margin-top: 10px;\n");
      out.write("            }\n");
      out.write("            .Item img{\n");
      out.write("                width: 250px;\n");
      out.write("                height: 260px;\n");
      out.write("                float: left;\n");
      out.write("                \n");
      out.write("                max-width:100%;\n");
      out.write("                padding:4px;\n");
      out.write("                line-height:1.42857143;\n");
      out.write("                background-color:#fff;\n");
      out.write("\n");
      out.write("                -webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;transition:all .2s ease-in-out;\n");
      out.write("            }\n");
      out.write("            .book-p{\n");
      out.write("                font-size: 24px;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("            .index-gouwuche{\n");
      out.write("                width: 130px;\n");
      out.write("                height: 40px;\n");
      out.write("                border: 2px solid lightgreen;\n");
      out.write("                border-radius: 4px;\n");
      out.write("                float: right;\n");
      out.write("\n");
      out.write("                margin-right: 20px;;\n");
      out.write("                background-color: lightgreen\n");
      out.write("            }\n");
      out.write("            .index-gouwuche-p{\n");
      out.write("                font-size: 20px;\n");
      out.write("                color: #FFF;\n");
      out.write("                float: left;\n");
      out.write("                margin-left: 15px;\n");
      out.write("                line-height: 40px;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("\n");
      out.write("        <img id=\"img2\" src=\"img/logo.png\">\n");
      out.write("        <div id=\"hello\">Hello</div><div id=\"world\">World</div>\n");
      out.write("\n");
      out.write("        <div id=\"login\"><div id=\"login-p\">登录</div></div>\n");
      out.write("        <div id=\"gouwuche\"><img src=\"img/gouwuche.png\" style=\"width: 30px; height: 30px; float: left; margin-left: 5px; margin-top: 2px;\"><p id=\"gouwuche-p\">购物车</p></div>\n");
      out.write("        <div id=\"clear\"></div>\n");
      out.write("        <hr>\n");
      out.write("        <div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\" data-interval=\"2000\">\n");
      out.write("            <!-- Indicators -->\n");
      out.write("            <ol class=\"carousel-indicators\">\n");
      out.write("                <li data-target=\"#carousel-example-generic\" data-slide-to=\"0\" class=\"active\"></li>\n");
      out.write("                <li data-target=\"#carousel-example-generic\" data-slide-to=\"1\"></li>\n");
      out.write("                <li data-target=\"#carousel-example-generic\" data-slide-to=\"2\"></li>\n");
      out.write("            </ol>\n");
      out.write("\n");
      out.write("            <!-- Wrapper for slides -->\n");
      out.write("            <div class=\"carousel-inner\" role=\"listbox\">\n");
      out.write("                <div class=\"item active\">\n");
      out.write("                    <img src=\"images/lunbo2.jpg\"  style=\"width: 100%; height: 400px;\">\n");
      out.write("                    <div class=\"carousel-caption\">\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"item\">\n");
      out.write("                    <img src=\"images/lunbo3.jpg\" style=\"width: 100%; height: 400px;\">\n");
      out.write("                    <div class=\"carousel-caption\">\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"item\">\n");
      out.write("                    <img src=\"images/17.jpg\" style=\"width: 100%; height: 400px;\">\n");
      out.write("                    <div class=\"carousel-caption\">\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <!-- Controls -->\n");
      out.write("            <a class=\"left carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"prev\">\n");
      out.write("                <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>\n");
      out.write("                <span class=\"sr-only\">Previous</span>\n");
      out.write("            </a>\n");
      out.write("            <a class=\"right carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"next\">\n");
      out.write("                <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>\n");
      out.write("                <span class=\"sr-only\">Next</span>\n");
      out.write("            </a>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"allbooks\">\n");
      out.write("            ");

                //获取请求对象中的属性all_movies，属性值是一个Movie对象列表
                List<Book> list = (List<Book>) request.getAttribute("all_books");
                //遍历电影，每个电影用一个class为item的a元素显示，在对应的位置显示电影的对应信息即可
                for (Book b:list) {
            
      out.write("\n");
      out.write("            <div class=\"Item\">\n");
      out.write("                <div>\n");
      out.write("                    <img src=\"img/book1.jpg\" >\n");
      out.write("                </div>\n");
      out.write("                <p class=\"book-p\">\n");
      out.write("                    &nbsp;&nbsp;&nbsp;书名：");
      out.print(b.getBookname());
      out.write("\n");
      out.write("                </p>\n");
      out.write("                <p class=\"book-p\">\n");
      out.write("                    &nbsp;&nbsp;&nbsp;作者：\n");
      out.write("                </p>\n");
      out.write("                <p class=\"book-p\">\n");
      out.write("                    &nbsp;&nbsp;&nbsp;价格：\n");
      out.write("                </p>\n");
      out.write("                <p class=\"book-p\">\n");
      out.write("                    &nbsp;&nbsp;&nbsp;类型：\n");
      out.write("                </p>\n");
      out.write("                <p class=\"book-p\">\n");
      out.write("                    &nbsp;&nbsp;&nbsp;库存：\n");
      out.write("                </p>\n");
      out.write("                <div class=\"index-gouwuche\"><p class=\"index-gouwuche-p\">加入购物车<p></div>\n");
      out.write("            </div>\n");
      out.write("            ");

                                }
            
      out.write("\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
