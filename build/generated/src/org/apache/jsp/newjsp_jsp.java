package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class newjsp_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n");
      out.write("    <title>Document</title>\n");
      out.write("    <style>\n");
      out.write("        #hello{\n");
      out.write("                font-family: ‘Courier New’, Courier, monospace;\n");
      out.write("                font-size: 40px;\n");
      out.write("                color: lightgreen;\n");
      out.write("                float: left;\n");
      out.write("                font-weight: bold;\n");
      out.write("                margin-top: 15px;\n");
      out.write("            }\n");
      out.write("            #world{\n");
      out.write("                font-family: ‘Courier New’, Courier, monospace;\n");
      out.write("                font-size: 40px;\n");
      out.write("                float: left;\n");
      out.write("                margin-top: 15px;\n");
      out.write("\n");
      out.write("\n");
      out.write("            }\n");
      out.write("            #login{\n");
      out.write("                width: 100px;\n");
      out.write("                height: 40px;\n");
      out.write("                border: 2px solid lightgreen;\n");
      out.write("                border-radius: 4px;\n");
      out.write("                float: right;\n");
      out.write("                margin-top: 20px;\n");
      out.write("                margin-right: 10%;\n");
      out.write("            }\n");
      out.write("             #login-p{\n");
      out.write("                font-size: 20px;\n");
      out.write("                color: lightgreen;\n");
      out.write("                text-align: center;\n");
      out.write("                line-height: 40px;\n");
      out.write("            }\n");
      out.write("            #img2{\n");
      out.write("                width: 30px;\n");
      out.write("                height: 30px;\n");
      out.write("                float: left;\n");
      out.write("                margin-top: 25px;\n");
      out.write("                margin-left: 10%;}\n");
      out.write("            #clear{\n");
      out.write("                clear: both;\n");
      out.write("            }\n");
      out.write("            #gouwuche{\n");
      out.write("                width: 130px;\n");
      out.write("                height: 40px;\n");
      out.write("                border: 2px solid lightgreen;\n");
      out.write("                border-radius: 4px;\n");
      out.write("                float: right;\n");
      out.write("                margin-top: 20px;\n");
      out.write("                margin-right: 1%;\n");
      out.write("                background-color: lightgreen\n");
      out.write("            }\n");
      out.write("            #gouwuche-p{\n");
      out.write("                font-size: 20px;\n");
      out.write("                color: #FFF;\n");
      out.write("                float: left;\n");
      out.write("                margin-left: 15px;\n");
      out.write("                line-height: 40px;\n");
      out.write("            }\n");
      out.write("       \n");
      out.write("\n");
      out.write("        .wrap {\n");
      out.write("            width: 250px;\n");
      out.write("            height: 250px;\n");
      out.write("            position: relative;\n");
      out.write("            margin-left: 100px;\n");
      out.write("            margin-top: 100px;\n");
      out.write("        }\n");
      out.write("\n");
      out.write("        .inner {\n");
      out.write("        \t/* 镜片宽度x/小图宽度=显示大图宽度/大图实际宽度 */\n");
      out.write("            /* x/250=300/500 */\n");
      out.write("            width: 150px;\n");
      out.write("            height: 150px;\n");
      out.write("            background-color: rgba(0, 0, 0, 0.5);\n");
      out.write("            position: absolute;\n");
      out.write("            top: 0;\n");
      out.write("            left: 0;\n");
      out.write("            display: none;\n");
      out.write("        }\n");
      out.write("\n");
      out.write("        .big {\n");
      out.write("            width: 500px;\n");
      out.write("            height: 500px;\n");
      out.write("            position: absolute;\n");
      out.write("            left: 270px;\n");
      out.write("            top: -20px;\n");
      out.write("            overflow: hidden;\n");
      out.write("            display: none;\n");
      out.write("        }\n");
      out.write("\n");
      out.write("    </style>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("    <img id=\"img2\" src=\"img/logo.png\">\n");
      out.write("        <div id=\"hello\">Hello</div><div id=\"world\">World</div>\n");
      out.write("\n");
      out.write("        <div id=\"login\"><div id=\"login-p\">登录</div></div>\n");
      out.write("        <div id=\"gouwuche\"><img src=\"img/gouwuche.png\" style=\"width: 30px; height: 30px; float: left; margin-left: 5px; margin-top: 2px;\"><p id=\"gouwuche-p\">购物车</p></div>\n");
      out.write("        <div id=\"clear\"></div>\n");
      out.write("        <hr>\n");
      out.write("   <div id=\"list_box\">\n");
      out.write("       <div class=\"wrap\">\n");
      out.write("        <!-- 小图片 -->\n");
      out.write("        <img src=\"img/book1.jpg\">\n");
      out.write("        <!-- 小图片内的镜片 -->\n");
      out.write("        <div class=\"inner\"></div>\n");
      out.write("        <!-- 大图片显示区域 -->\n");
      out.write("        <div class=\"big\">\n");
      out.write("            <!-- 大图片 -->\n");
      out.write("            <img src=\"img/book1.jpg\" style=\"width: 500px; height: 500px;\">\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    </div>    <script>\n");
      out.write("       var wrap = document.querySelector('.wrap');\n");
      out.write("        var inner = document.querySelector('.inner');\n");
      out.write("        var big = document.querySelector('.big');\n");
      out.write("        var bigImg = document.querySelector('.big img');\n");
      out.write("        // 鼠标移动事件\n");
      out.write("        wrap.onmousemove = function (e) {\n");
      out.write("            var e = window.event || e;\n");
      out.write("            // 获取元素相对于浏览器窗口的坐标e.clientX-镜片一半的距离-相对于body的偏移量\n");
      out.write("            var left = e.clientX - inner.offsetWidth / 2 - wrap.offsetLeft;\n");
      out.write("            var top = e.clientY - inner.offsetHeight / 2 - wrap.offsetTop;\n");
      out.write("            // console.log(left, top);\n");
      out.write("            // 控制镜片移动的最大边界；\n");
      out.write("            var bigWidth = wrap.offsetWidth - inner.offsetWidth;\n");
      out.write("            var bigHeight = wrap.offsetHeight - inner.offsetHeight;\n");
      out.write("            // console.log(bigWidth, bigHeight);\n");
      out.write("            // 如果鼠标移动的最大值大于最大边界，限定最大值为最大边界；\n");
      out.write("            if (left > bigWidth) {\n");
      out.write("                left = bigWidth;\n");
      out.write("            }\n");
      out.write("            if (left < 0) {\n");
      out.write("                left = 0;\n");
      out.write("            }\n");
      out.write("            if (top > bigHeight) {\n");
      out.write("                top = bigHeight;\n");
      out.write("            }\n");
      out.write("            if (top < 0) {\n");
      out.write("                top = 0;\n");
      out.write("            }\n");
      out.write("            // 控制镜片的位置\n");
      out.write("            inner.style.left = left + \"px\";\n");
      out.write("            inner.style.top = top + 'px';\n");
      out.write("            // 控制大图片移动相对位置(-大图的宽度/小图的宽度*镜片的偏移量)\n");
      out.write("            bigImg.style.marginLeft = -500 / 250 * inner.offsetLeft + 'px';\n");
      out.write("            bigImg.style.marginTop = -500 / 250 * inner.offsetTop + 'px';\n");
      out.write("        }\n");
      out.write("        wrap.onmouseenter = function () {\n");
      out.write("            inner.style.display = 'block';\n");
      out.write("            big.style.display = 'block';\n");
      out.write("        }\n");
      out.write("        wrap.onmouseleave = function () {\n");
      out.write("            inner.style.display = 'none';\n");
      out.write("            big.style.display = 'none';\n");
      out.write("        }\n");
      out.write("\n");
      out.write("    </script>\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
